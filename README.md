# csdn资源下载工具

#### 介绍
+ csdn 免积分下载工具
+ csdn 免会员下载工具
+ csdn vip资源下载工具
+ csdn 文章下载，免积分，免购买超级会员


#### 软件架构
采用spingboot + dubbo 架构设计 

#### 安装教程

1.  体验地址 https://mark.cuckooing.cn/

#### 使用说明

1. 复制需要下载页面的url(也即是浏览器地址栏的地址)
2. 张贴到下载输入框的
3. 点击下载

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request











